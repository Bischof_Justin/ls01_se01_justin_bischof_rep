import java.util.Scanner;

public class MethodenBeispiele {
	
	public static String text;
	
	public static void main(String[] args) {
		
		int zahl1 = 4;
		int zahl2 = 5;
		
		int erg = add2(zahl1, zahl2);
		System.out.println("Ergebnis: " + erg);
		
		double erg2 = hochzwei(zahl1);
		System.out.println("Ergebnis: " + erg);
		
		eingabe();
		hochzwei2();
		
	}
	
	public static void hochzwei2() {
		Scanner myScanner = new Scanner(System.in);
		
		System.out.println("\n\nPasswort: ");
		text = myScanner.next();
		if(text.equals("login")) {
			System.out.println("\nRichtig");
			
		} else {
			System.out.println("\nFalsch");
		}
	}
	
	public static double hochzwei(double zahl) {
		double result = zahl * zahl;
		return result;
	}
	
	public static int add2(int zahl1, int zahl2) {
		int result = zahl1 + zahl2;
		return result;
	}
	
	public static void add(double zahl1, double zahl2) {
		System.out.println(zahl1 + zahl2);
		System.out.printf("%.2f�", zahl1 + zahl2);
	}
	
	public static double eingabe() {
		Scanner myscanner = new Scanner(System.in);
		System.out.println("Tragen Sie eine Zahl ein: ");
		double eingabe = myscanner.nextDouble();
		System.out.printf("Ihre Zahl ist: %.2f", eingabe);
		return eingabe;
	}
	
}
