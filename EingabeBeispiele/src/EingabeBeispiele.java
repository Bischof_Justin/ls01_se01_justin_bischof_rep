import java.util.Scanner;

public class EingabeBeispiele {

	public static void main(String[] args) {
		
		Scanner myScanner = new Scanner(System.in);
		
		System.out.println("Geben Sie bitte eine Zahl ein: ");
		int zahl1 = myScanner.nextInt();
		System.out.printf("Deine Zahl ist %s", zahl1 + ".");
		
		System.out.println("\nGeben Sie bitte Ihren Vorname ein: ");
		String vorname = myScanner.next();
		System.out.println("Dein Vorname lautet: " + vorname + ".");
		
		System.out.println("Geben Sie bitte ein Symbol ein: ");
		char symbol = myScanner.next().charAt(0);
		System.out.println("Symbol: " + symbol + ".");
	}

}
