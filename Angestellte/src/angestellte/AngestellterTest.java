package angestellte;

public class AngestellterTest {

	public static void main(String[] args) {
		Angestellter a1 = new Angestellter();
		
//		a1.name = "Max Mustermann";
//		a1.gehalt = 1000.50;
		
//		a1.setName("Max Mustermann");
//		a1.setGehalt(1000);
		
		System.out.println(a1.getName());
		System.out.println(a1.getGehalt());
		
		Angestellter a2 = new Angestellter("Anna Musterfrau", 8000.0);
		
		System.out.println(a2.getName());
		System.out.println(a2.getGehalt());
	}

}
