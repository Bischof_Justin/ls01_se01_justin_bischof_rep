
public class ArrayBeispiele {

	public static void main(String[] args) {
		
		Aufgabe1();
		Aufgabe2();
	}
	
	public static void Aufgabe1() {
		
		int[] zliste = new int[10];
		
		for(int i = 0; i < zliste.length; i++) {
			zliste[i] = i + 1;
		}
		
		for(int i = 0; i < zliste.length; i++) {
			System.out.print(zliste[i] + " ");
		}
		
		System.out.println("\nAufgabe 1 wurden ausgegeben.\n");
	}
	
	public static void Aufgabe2() {
		
		int[] ungeradeZahl = new int[10];
		
		for(int i = 0; i < ungeradeZahl.length; i++) {
			ungeradeZahl[i] = 2 * i + 1;
		}
		
		for(int i = 0; i < ungeradeZahl.length; i++) {
			System.out.print(ungeradeZahl[i] + " ");
		}
		
		System.out.println("\nAufgabe 2 wurden ausgegeben.\n");
	}

}
