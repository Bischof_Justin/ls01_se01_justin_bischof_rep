package anfaenger;

import java.util.Scanner;

public class Ladung {
	
	public static Ladung klingonenLadung = new Ladung();
	public static Ladung klingonenLadung2 = new Ladung();
	public static Ladung romulanerLadung = new Ladung();
	public static Ladung romulanerLadung2 = new Ladung();
	public static Ladung romulanerLadung3 = new Ladung();
	public static Ladung vulkanierLadung = new Ladung();
	public static Ladung vulkanierLadung2 = new Ladung();
	
	public static Scanner tastatur = new Scanner(System.in);
	
	private String bezeichnung;
	private int menge;
	
	public Ladung() {
		this.bezeichnung = "unknown";
		this.menge = 0;
	}
	
	public Ladung(String bezeichnung, int menge) {
		this.bezeichnung = bezeichnung;
		this.menge = menge;
	}
	
	public void setBezeichnung(String bezeichnung) {
		this.bezeichnung = bezeichnung;
	}
	
	public String getBezeichnung() {
		return bezeichnung;
	}
	
	public void setMenge(int menge) {
		this.menge = menge;
	}
	
	public int getMenge() {
		return menge;
	}
	
}
