package anfaenger;

import java.util.Scanner;

public class RaumschiffTest {
	
	public static Raumschiff klingonen = new Raumschiff();
	public static Raumschiff romulaner = new Raumschiff();
	public static Raumschiff vulkanier = new Raumschiff();
	
	public static Ladung klingonenLadung = new Ladung();
	public static Ladung klingonenLadung2 = new Ladung();
	public static Ladung romulanerLadung = new Ladung();
	public static Ladung romulanerLadung2 = new Ladung();
	public static Ladung romulanerLadung3 = new Ladung();
	public static Ladung vulkanierLadung = new Ladung();
	public static Ladung vulkanierLadung2 = new Ladung();
	
	public static Raumschiff raumschiff = new Raumschiff();
	
	public static Scanner tastatur = new Scanner(System.in);

	public static void main(String[] args) {
		
		klingonen = new Raumschiff(1, 100, 100, 100, 100, "IKS Hegh'ta", 2, "Klingonen");
		romulaner = new Raumschiff(2, 100, 100, 100, 100, "IRW Khazara", 2, "Romulaner");
		vulkanier = new Raumschiff(0, 80, 80, 50, 100, "Ni'Var", 5, "Vulkanier");
		
		klingonenLadung = new Ladung("Ferengi Schneckensaft", 200);
		klingonenLadung2 = new Ladung("Bat*leth Klingonen Schwert", 200);
		romulanerLadung = new Ladung("Borg-Schrott", 5);
		romulanerLadung2 = new Ladung("Rote Materie", 2);
		romulanerLadung3 = new Ladung("Plasma-Waffe", 50);
		vulkanierLadung = new Ladung("Forschungssonde", 35);
		vulkanierLadung2 = new Ladung("Photonentorpedo", 3);
		
		klingonen.addLadung(klingonenLadung);
		klingonen.addLadung(klingonenLadung2);
		romulaner.addLadung(romulanerLadung);
		romulaner.addLadung(romulanerLadung2);
		romulaner.addLadung(romulanerLadung3);
		vulkanier.addLadung(vulkanierLadung);
		vulkanier.addLadung(vulkanierLadung2);
		
		// Klingonen schie�en mit dem Photonentorpedo einmal auf die Romulaner \\
		System.out.println("Die Klingonen schie�en mit dem Photonentorpedo auf die Romulaner...");
		romulaner.photonentorpedoSchiessen(klingonen);
		ladebildschirm();
		
		// Romulaner schie�en mit der Phaserkanone auf die Klingonen zur�ck \\
		System.out.println("Die Romulaner schie�en mit der Phaserkanone auf die Klingonen zur�ck...");
		klingonen.phaserkanoneSchiessen(romulaner);
		ladebildschirm();
		
		// Vulkanier sendet an allen eine Nachricht \\
		vulkanier.nachrichtAnAlle("Vulkanier -> Gewalt ist nicht logisch.");
		ladebildschirm();
		
		// Klingonen ruft ihren Zustand und ihre Ladung aus \\
		raumschiff.zustandRaumschiff(klingonen);
		ladebildschirm();
		
		// Klingonen schie�en mit zwei weiteren Photonentorpedo auf die Romulaner \\
		System.out.println("Die Klingonen schie�en mit dem Photonentorpedo auf die Romulaner...");
		romulaner.photonentorpedoSchiessen(klingonen);
		System.out.println("\nDie Klingonen schie�en mit dem Photonentorpedo auf die Romulaner...");
		romulaner.photonentorpedoSchiessen(klingonen);
		ladebildschirm();
		
		// Klingonen, die Romulaner und die Vulkanier rufen jeweils den Zustand Ihres Raumschiffes ab und geben Ihr Ladungsverzeichnis aus \\
		
		raumschiff.zustandRaumschiff(klingonen);
		System.out.println("\n");
		raumschiff.zustandRaumschiff(romulaner);
		System.out.println("\n");
		raumschiff.zustandRaumschiff(vulkanier);
		ladebildschirm();
		
		// LOGS \\
		raumschiff.eintraegeLogbuchZurueckgeben();
	}
	
	public static void ladebildschirm() {
	       for(int i = 0; i < 15; i++) {
	          System.out.print("=");
	          try {
				Thread.sleep(250);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
	       }
	       
	       System.out.println("\n");
	}

}
