package anfaenger;

import java.util.ArrayList;

public class Raumschiff {
	
	private int photonentorpedoAnzahl;
	private int energieversorgungInProzent;
	private int schildeInProzent;
	private int huelleInProzent;
	private int lebenserhaltungssystemInProzent;
	private int androidenAnzahl;
	private String schiffsname;
	private String raumschiffsname;
	private static ArrayList<String> broadcastKommunikator = new ArrayList<String>();
	private ArrayList<Ladung> ladungsverzeichnis = new ArrayList<Ladung>();
	
	public Raumschiff() {
		this.photonentorpedoAnzahl = 0;
		this.energieversorgungInProzent = 0;
		this.schildeInProzent = 0;
		this.huelleInProzent = 0;
		this.lebenserhaltungssystemInProzent = 0;
		this.schiffsname = "unknown";
		this.androidenAnzahl = 0;
		this.raumschiffsname = "unknown";
	}
	
	public Raumschiff(int photonentorpedoAnzahl, int energieversorgungInProzent, int zustandSchildeInProzent, int zustandHuelleInProzent, int zustandLebenserhaltungssystemInProzent,
			String schiffsname, int anzahlDroiden, String raumschiffsname) {
		this.photonentorpedoAnzahl = photonentorpedoAnzahl;
		this.energieversorgungInProzent = energieversorgungInProzent;
		this.schildeInProzent = zustandSchildeInProzent;
		this.huelleInProzent = zustandHuelleInProzent;
		this.lebenserhaltungssystemInProzent = zustandLebenserhaltungssystemInProzent;
		this.schiffsname = schiffsname;
		this.androidenAnzahl = anzahlDroiden;
		this.raumschiffsname = raumschiffsname;
	}
	
	public int getPhotonentorpedoAnzahl() {
		return photonentorpedoAnzahl;
	}
	
	public void setPhotonentorpedoAnzahl(int photonentorpedoAnzahl) {
		this.photonentorpedoAnzahl = photonentorpedoAnzahl;
	}
	
	public int getEnergieversorgungInProzent() {
		return energieversorgungInProzent;
	}
	
	public void setEnergieversorgungInProzent(int zustandEnergieversorgungInProzent) {
		this.energieversorgungInProzent = zustandEnergieversorgungInProzent;
	}
	
	public int getSchildeInProzent() {
		return schildeInProzent;
	}
	
	public void setSchildeInProzent(int zustandSchildeInProzent) {
		this.schildeInProzent = zustandSchildeInProzent;
	}
	
	public int getHuelleInProzent() {
		return huelleInProzent;
	}
	
	public void setHuelleInProzent(int zustandHuelleInProzent) {
		this.huelleInProzent = zustandHuelleInProzent;
	}
	
	public int getLebenserhaltungssystemInProzent() {
		return lebenserhaltungssystemInProzent;
	}
	
	public void setLebenserhaltungssystemInProzent(int zustandLebenserhaltungssystemInProzent) {
		this.lebenserhaltungssystemInProzent = zustandLebenserhaltungssystemInProzent;
	}
	
	public int getAndroidAnzahl() {
		return androidenAnzahl;
	}
	
	public void setAndroidenAnzahl(int androidenAnzahl) {
		this.androidenAnzahl = androidenAnzahl;
	}
	
	public String getSchiffsname() {
		return schiffsname;
	}
	
	public void setSchiffsname(String schiffsname) {
		this.schiffsname = schiffsname;
	}
	
	public String getRaumschiffsname() {
		return raumschiffsname;
	}
	
	public void addLadung(Ladung neueLadung) {
		ladungsverzeichnis.add(neueLadung);
	}
	
	public void photonentorpedoSchiessen(Raumschiff raumschiffsname) {
		if(raumschiffsname.getPhotonentorpedoAnzahl() >= 1) {
			raumschiffsname.setPhotonentorpedoAnzahl(raumschiffsname.getPhotonentorpedoAnzahl() - 1);
			nachrichtAnAlle("Photonentorpedo abgeschossen!");
			System.out.println("Das Raumschiff " + raumschiffsname.getRaumschiffsname() + " hat noch " + raumschiffsname.getPhotonentorpedoAnzahl() + " Torpedos �brig.");
			treffer(raumschiffsname);
			
		} else if(raumschiffsname.getPhotonentorpedoAnzahl() == 0) {
			nachrichtAnAlle("-=*Click*=-");
		}
		
		return;
	}
	
	public void phaserkanoneSchiessen(Raumschiff raumschiffsname) {
		if(raumschiffsname.getEnergieversorgungInProzent() >= 50) {
			raumschiffsname.setEnergieversorgungInProzent(raumschiffsname.getEnergieversorgungInProzent() - 50);
			nachrichtAnAlle("Phaserkanone abgeschossen!");
			treffer(raumschiffsname);
			
		} else {
			nachrichtAnAlle("-=*Click*=-");
		}
		
		return;
	}
	
	private void treffer(Raumschiff raumschiffsname) {
		System.out.println("\n[" + getRaumschiffsname() + "] wurden getroffen!");
		if(getSchildeInProzent() >= 50) {
			setSchildeInProzent(getSchildeInProzent() - 50);
			
		} else if(getSchildeInProzent() == 0) {
			setHuelleInProzent(getHuelleInProzent() - 50);
			setEnergieversorgungInProzent(getEnergieversorgungInProzent() - 50);
		}
		
		if(getHuelleInProzent() == 0) {
			setLebenserhaltungssystemInProzent(0);
			nachrichtAnAlle("[" + getRaumschiffsname() + "] Lebenserhaltungssysteme worden vernichtet.");
		}
		
		return;
	}
	
	public void nachrichtAnAlle(String message) {
		broadcastKommunikator.add(message);
		System.out.println("[Nachricht] " + message);
		return;
	}
	
	public void eintraegeLogbuchZurueckgeben() {
		System.out.println("***** Logbuch *****");
		for(int i=0; i < broadcastKommunikator.size(); i++) {
			System.out.println(i + ": " + broadcastKommunikator.get(i));
		}
		
		System.out.println("\nGr��e des Logbuches: " + broadcastKommunikator.size());
		
		return;
	}
	
	public void photonentorpedosLaden(int anzahlTorpedos) {
		if(!(getPhotonentorpedoAnzahl() >= anzahlTorpedos)) {
			System.out.println("Keine Photonentorpedos gefunden!");
		}
		
		return;
	}
	
	public void reparaturDurchfuehren(boolean schutzschilde, boolean energieversorgung, boolean schiffshuelle, int anzahlDroiden) {
		return;
	}
	
	public void zustandRaumschiff(Raumschiff raumschiffsname) {
		System.out.println("***** Raumschiff " + raumschiffsname.getRaumschiffsname() + " *****");
		System.out.println("PhotonentorpedoAnzahl = " + raumschiffsname.getPhotonentorpedoAnzahl());
		System.out.println("EnergieversorgungInProzent = " + raumschiffsname.getEnergieversorgungInProzent());
		System.out.println("SchildeInProzent = " + raumschiffsname.getSchildeInProzent());
		System.out.println("HuelleInProzent = " + raumschiffsname.getHuelleInProzent());
		System.out.println("LebenserhaltungssystemInProzent = " + raumschiffsname.getLebenserhaltungssystemInProzent());
		System.out.println("Schiffsname = " + raumschiffsname.getSchiffsname());
		System.out.println("AndroidenAnzahl = " + raumschiffsname.getAndroidAnzahl());
		raumschiffsname.ladungsverzeichnisAusgeben();
	}
	
	public void ladungsverzeichnisAusgeben() {
		for(int i=0; i < ladungsverzeichnis.size(); i++) {
			System.out.println("Bezeichnung: " + ladungsverzeichnis.get(i).getBezeichnung() + ", Menge: " + ladungsverzeichnis.get(i).getMenge());
		}
		
		return;
	}
	
	public void ladungsverzeichnisAufraeumen() {
		ladungsverzeichnis.clear();
		return;
	}
}
