import java.util.Scanner;

public class PCHaendler {

	public static void main(String[] args) {
		
		String artikel = liesString("was m�chten Sie bestellen?");
		int anzahl = liesInt("Geben Sie die Anzahl ein:");
		double nettopreis = liesDoubleNetto("Geben Sie den Nettopreis ein:");
		double mwst = liesDoubleWert("Geben Sie den Mehrwertsteuersatz in Prozent ein:");
		double nettogesamtpreis = berechneGesamtnettopreis(anzahl, nettopreis);
		double bruttogesamtpreis = berechneGesamtbruttopreis(nettogesamtpreis, mwst);
		rechnungausgeben(artikel, anzahl, nettogesamtpreis, bruttogesamtpreis, mwst);
	}
	
	public static String liesString(String text) {
		Scanner myScanner = new Scanner(System.in);
		System.out.println(text);
		String artikel = myScanner.next();
		return artikel;
	}
	
	public static int liesInt(String text) {
		Scanner myScanner = new Scanner(System.in);
		System.out.println(text);
		int anzahl = myScanner.nextInt();
		return anzahl;
	}
	
	public static double liesDoubleNetto(String text) {
		Scanner myScanner = new Scanner(System.in);
		System.out.println(text);
		double preis = myScanner.nextDouble();
		return preis;
	}
	
	public static double liesDoubleWert(String text) {
		Scanner myScanner = new Scanner(System.in);
		System.out.println(text);
		double mwst = myScanner.nextDouble();
		return mwst;
	}
	
	public static double berechneGesamtnettopreis(int anzahl, double nettopreis) {
		double nettogesamtpreis = anzahl * nettopreis;
		return nettogesamtpreis;
	}
	
	public static double berechneGesamtbruttopreis(double nettogesamtpreis, double mwst) {
		double bruttogesamtpreis = nettogesamtpreis * (1 + mwst / 100);
		return bruttogesamtpreis;
	}
	
	public static void rechnungausgeben(String artikel, int anzahl, double nettogesamtpreis, double bruttogesamtpreis, double mwst) {
		System.out.println("\t|Rechnung|");
		System.out.printf("\t\t Netto:  %-20s %6d %10.2f %n", artikel, anzahl, nettogesamtpreis);
		System.out.printf("\t\t Brutto: %-20s %6d %10.2f (%.1f%s)%n", artikel, anzahl, bruttogesamtpreis, mwst, "%");
	}

}
