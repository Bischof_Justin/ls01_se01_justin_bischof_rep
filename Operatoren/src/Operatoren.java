﻿
public class Operatoren {
  public static void main(String [] args){
    /* 1. Vereinbaren Sie zwei Ganzzahlen.*/
	  
	  int zahl1, zahl2;

    System.out.println("UEBUNG ZU OPERATOREN IN JAVA\n");
    /* 2. Weisen Sie den Ganzzahlen die Werte 75 und 23 zu
          und geben Sie sie auf dem Bildschirm aus. */
    
    zahl1 = 75;
    zahl2 = 23;
    
    System.out.println("Zahl 1 ist " + zahl1 + " und Zahl 2 ist " + zahl2 + ".");

    /* 3. Addieren Sie die Ganzzahlen
          und geben Sie das Ergebnis auf dem Bildschirm aus. */
    
    int zahl3 = zahl1 + zahl2;
    System.out.println(zahl3);

    /* 4. Wenden Sie alle anderen arithmetischen Operatoren auf die
          Ganzzahlen an und geben Sie das Ergebnis jeweils auf dem
          Bildschirm aus. */
    
    int zahl4 = zahl1 + zahl2;
    int zahl5 = zahl1 - zahl2;
    int zahl6 = zahl1 * zahl2;
    int zahl7 = zahl1 / zahl2;
    int zahl8 = zahl1 % zahl2;
    
    System.out.println(zahl4);
    System.out.println(zahl5);
    System.out.println(zahl6);
    System.out.println(zahl7);
    System.out.println(zahl8);

    /* 5. Ueberprüfen Sie, ob die beiden Ganzzahlen gleich sind
          und geben Sie das Ergebnis auf dem Bildschirm aus. */
    
    if(zahl1 == zahl2) {
    	System.out.println(true);
    	
    } else {
    	System.out.println(false);
    }

    /* 6. Wenden Sie drei anderen Vergleichsoperatoren auf die Ganzzahlen an
          und geben Sie das Ergebnis jeweils auf dem Bildschirm aus. */

    /* 7. Ueberprüfen Sie, ob die beiden Ganzzahlen im  Interval [0;50] liegen
          und geben Sie das Ergebnis auf dem Bildschirm aus. */
          
  }//main
}// Operatoren
