import java.util.Scanner;

public class WhileSchleifeBeispiele {

	public static void main(String[] args) {
		Scanner myScanner = new Scanner(System.in);
		
		System.out.println("Bis welche Zahl soll ausgegeben werden? ");
		int zahl = myScanner.nextInt();
		
		int n = 1;
		while(n <= zahl) {
			System.out.print(n);
			if(n < zahl) {
				System.out.print(", ");
			}
			
			n++;
		}
	}
}
