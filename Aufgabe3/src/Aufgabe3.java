
public class Aufgabe3 {

	public static void main(String[] args) {
		
		System.out.printf("%s%3s%10s\n", "Fahrenheit", "|", "Celsius");
		System.out.printf("%s\n", "------------------------");
		System.out.printf("%s%10s%10s\n", "-20", "|", "-28.89");
		System.out.printf("%s%10s%10s\n", "-10", "|", "-23.33");
		System.out.printf("%s%11s%10s\n", "+0", "|", "-17.78");
		System.out.printf("%s%10s%10s\n", "+20", "|", "-6.67");
		System.out.printf("%s%10s%10s", "+30", "|", "-1.11");
	}

}
