
public class WeltDerZahlen {

	public static void main(String[] args) {
		
	    /*  *********************************************************
	    
        Zuerst werden die Variablen mit den Werten festgelegt!
   
        *********************************************************** */
		
	    // Im Internet gefunden ?
	    // Die Anzahl der Planeten in unserem Sonnesystem 
		int anzahlPlaneten = 8;
		
		// Anzahl der Sterne in unserer Milchstra�e
		int anzahlSterne = 0;
		
		// Wie viele Einwohner hat Berlin?
		int bewohnerBerlin = 0;
		
		// Wie alt bist du?  Wie viele Tage sind das?
		int alterTage = 0;
		
	    // Wie viel wiegt das schwerste Tier der Welt?
	    // Schreiben Sie das Gewicht in Kilogramm auf!
		int gewichtKilogramm = 0;
		
		// Schreiben Sie auf, wie viele km� das gr��te Land der Erde hat?
		int flaecheGroessteLand = 0;
		
		// Wie gro� ist das kleinste Land der Erde?
		int flaecheKleinsteLand = 0;
		
		
	    /* *********************************************************
	    
        Programmieren Sie jetzt die Ausgaben der Werte in den Variablen
   
        *********************************************************** */
		
	    System.out.println("Anzahl der Planeten: " + anzahlPlaneten);
	    System.out.println("Anzahl der Sterne: " + anzahlSterne);
	    System.out.println("Anzahl der Bewohner: " + bewohnerBerlin);
	    System.out.println("Anzahl der Tage meines Alters: " + alterTage);
	    System.out.println("Gewicht: " + gewichtKilogramm);
	    System.out.println("Gr��te: " + flaecheGroessteLand);
	    System.out.println("Kleinste: " + flaecheKleinsteLand);
	    System.out.println(" *******  Ende des Programms  ******* ");

	}

}
