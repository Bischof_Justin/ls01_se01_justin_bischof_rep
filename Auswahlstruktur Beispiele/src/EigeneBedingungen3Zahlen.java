import java.util.Scanner;

public class EigeneBedingungen3Zahlen {

	@SuppressWarnings("resource")
	public static void main(String[] args) {
		
		Scanner myScanner = new Scanner(System.in);
		
		System.out.println("Bitte geben Sie die 1. Zahl ein: ");
		int zahl1 = myScanner.nextInt();
		
		System.out.println("Bitte geben Sie die 2. Zahl ein: ");
		int zahl2 = myScanner.nextInt();
		
		System.out.println("Bitte geben Sie die 3. Zahl ein: ");
		int zahl3 = myScanner.nextInt();
		
		kleinsteZahl(zahl1, zahl2, zahl3);
	}
	
	public static void kleinsteZahl(int zahl1, int zahl2, int zahl3) {
		if(zahl1 < zahl2 && zahl1 < zahl3) {
			System.out.println("Die Zahl " + zahl1 + " ist kleiner.");
		}
		
		if(zahl2 < zahl1 && zahl2 < zahl3) {
			System.out.println("Die Zahl " + zahl2 + " ist kleiner.");
		}
		
		if(zahl3 < zahl1 && zahl3 < zahl2) {
			System.out.println("Die Zahl " + zahl3 + " ist kleiner.");
		}
	}

}
