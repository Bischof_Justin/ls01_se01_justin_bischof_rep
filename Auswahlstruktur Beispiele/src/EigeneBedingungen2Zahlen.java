import java.util.Scanner;

public class EigeneBedingungen2Zahlen {

	@SuppressWarnings("resource")
	public static void main(String[] args) {
		
		Scanner myScanner = new Scanner(System.in);
		
		System.out.println("Bitte geben Sie die 1. Zahl ein: ");
		int zahl1 = myScanner.nextInt();
		
		System.out.println("Bitte geben Sie die 2. Zahl ein: ");
		int zahl2 = myScanner.nextInt();
		
		kleinsteZahl(zahl1, zahl2);

	}
	
	public static void kleinsteZahl(int zahl1, int zahl2) {
		if(zahl1 > zahl2) {
			System.out.println("Die Zahl " + zahl1 + " ist gr��er.");
			
		} else {
			System.out.println("Die Zahl " + zahl2 + " ist gr��er.");
		}
	}

}
