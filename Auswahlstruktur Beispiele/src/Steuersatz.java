import java.util.Scanner;

public class Steuersatz {

	@SuppressWarnings("unused")
	public static void main(String[] args) {
		
		Scanner myScanner = new Scanner(System.in);
		System.out.println("Bitte geben Sie den Nettowert an: ");
		double nettowert = myScanner.nextDouble();
		
		System.out.println("Wollen Sie den erm��igten (Ja) oder den vollen Steuersatz (Nein) anwenden?");
		String auswahl = myScanner.next();
		
		double brutto = 0;
		abfrage(auswahl, nettowert, brutto);
	}
	
	public static void abfrage(String auswahl, double nettowert, double brutto) {
		if(auswahl.equalsIgnoreCase("Ja")) {
			brutto = nettowert + 0.05 * nettowert;
			System.out.println("Ihr Bruttowert lautet: " + brutto + "�");
			
		} else if(auswahl.equalsIgnoreCase("Nein")) {
			brutto = nettowert + 0.16 * nettowert;
			System.out.println("Ihr Bruttowert lautet: " + brutto + "�");
		}
	}

}
