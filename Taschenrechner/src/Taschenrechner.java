import java.util.Scanner;

public class Taschenrechner {

	@SuppressWarnings("resource")
	public static void main(String[] args) {
		
		String euro = "EURO";
		Scanner tastatur = new Scanner(System.in);
		
		double ersterwert, zweiterwert, ergebnis;
		
		System.out.print("Bitte geben Sie hier einen Wert in Euro an: ");
		ersterwert = tastatur.nextDouble();
		
		System.out.print("\nBitte geben Sie hier noch einen Wert in Euro an: ");
		zweiterwert = tastatur.nextDouble();
		
		System.out.print("\nIhr Ergebnis wird brechnet...");
		System.out.printf("\n(%.2f %s %s %.2f %s%s", ersterwert, euro, "+", zweiterwert, euro, ")\n\n");
		ergebnis = ersterwert += zweiterwert;
		
		for(int i = 0; i < 13; i++) {
			System.out.print("= = ");
			
			try {
				Thread.sleep(250);
			} catch(InterruptedException e) {
				e.printStackTrace();
			}
		}
		
		System.out.printf("\n\nIhr Ergebnis lautet: %.2f %s", ergebnis, euro + ".");

	}

}
